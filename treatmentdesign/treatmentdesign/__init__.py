from .treatmentdesign import TreatmentDesign
from .parallel.slave import Slave
from .parallel.master import Master

import logging
module_logger = logging.getLogger('treatmentdesign')
module_logger.addHandler(logging.NullHandler())
