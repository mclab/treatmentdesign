import math

from backtrackingsolver.constraint import Constraint
from .clinicalaction import NoAction
from .clinicalaction import ClinicalAction
import utils


class MinDose(Constraint):
    def __init__(self, get_min_value, get_duration):
        self._name = 'MinDose'
        self._overall_drug_per_depth = [0]
        self.__get_min_value = get_min_value
        self.__get_duration = get_duration
        # self._time_slot = time_slot
        # self._depth_day_start_min = int(math.ceil(day_start_min / time_slot))

    def __update_drug_dose(self, action):
        overall_drug = self._overall_drug_per_depth[-1]
        if action != NoAction:
            overall_drug += action.dose()
        self._overall_drug_per_depth.append(overall_drug)
        return overall_drug

    def initialize(self, actions):
        self._overall_drug_per_depth = [0]
        for idx in range(1, len(actions) + 1):
            self.check(actions[:idx])

    def check(self, actions):
        depth = len(actions) - 1
        a = actions[-1]
        if depth < len(self._overall_drug_per_depth):
            self._overall_drug_per_depth = self._overall_drug_per_depth[:max(1, depth)]
        self.overall_drug = self.__update_drug_dose(a)
        # print(actions, len(actions), self.overall_drug, self.__get_min_value())
        if not math.isinf(self.__get_min_value()):
            return self.overall_drug < self.__get_min_value() and abs(self.overall_drug - self.__get_min_value()) > 0.001
            # and (not utils.isclose(self.overall_drug, self.__get_min_value(), rel_tol=1e-4))
        return True


# class Optimality(Constraint):
#     def __init__(self, alpha, beta, gamma, get_current_duration, get_min_value):
#         self._name = 'Optimality'
#         self._alpha = alpha
#         self._beta = beta
#         self._gamma = gamma
#         self._overall_drug_per_depth = []
#         self._min_drug_injs_per_depth = []
#         self._min_duration_per_depth = []
#         self.__get_current_duration = get_current_duration
#         self.__get_min_value = get_min_value
#         # print(self.__get_min_drug_dose())

#     def __update_drug_injs(self, action):
#         drug_injs = 0
#         if len(self._min_drug_injs_per_depth) > 0:
#             drug_injs = self._min_drug_injs_per_depth[-1]
#         if action != NoAction:
#             drug_injs += 1
#         self._min_drug_injs_per_depth.append(drug_injs)
#         return drug_injs

#     def __update_drug_dose(self, action):
#         overall_drug = 0.0
#         if len(self._overall_drug_per_depth) > 0:
#             overall_drug = self._overall_drug_per_depth[-1]
#         if action != NoAction:
#             overall_drug += action.dose()
#         self._overall_drug_per_depth.append(overall_drug)
#         return overall_drug

#     def initialize(self, actions):
#         self._overall_drug_per_depth = []
#         self._min_drug_injs_per_depth = []
#         if actions is not None:
#             for a in actions:
#                 self.__update_drug_dose(a)
#                 self.__update_drug_injs(a)

#     def check(self, actions):
#         depth = len(actions)
#         self._overall_drug_per_depth = self._overall_drug_per_depth[:depth-1]
#         self._min_drug_injs_per_depth = self._min_drug_injs_per_depth[:depth-1]
#         action = actions[-1]
#         self.overall_drug = self.__update_drug_dose(action)
#         self.drug_injs = self.__update_drug_injs(action)
#         self.duration = self.__get_current_duration(actions)
#         self.value = self.overall_drug * self._alpha + self.drug_injs * self._beta + self.duration * self._gamma
#         sat = self.value < self.__get_min_value()
#         # if MPI.COMM_WORLD.Get_rank() != 0:
#         #     print('[{}] {}: {} {} {} --> {}'.format(MPI.COMM_WORLD.Get_rank(), actions, overall_drug, drug_injs, duration, self.value))
#         return sat
