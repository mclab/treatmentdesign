import time
import logging

import backtrackingsolver
from paeon.treatment import Treatment
from paeon.treatment import Injection
from paeon import simulator 

import utils
from .clinicalaction import ClinicalAction
from .clinicalaction import NoAction
from .mindose import MinDose
from .downregulation import Downregulation


class TreatmentDesign(object):
    def __init__(self, day_start, duration, time_slot, below_threshold, max_sequence_no_actions,
                 initstates, species_thresholds, drugs, model_filenames, alpha, beta, gamma, random_ordering=False, outdir='.', snapshot=None):

        self._logger = logging.getLogger('treatmentdesign.TreatmentDesign')

        self._min_value = snapshot['drug_dose'] if snapshot is not None else float('inf')
        self._min_overall_drug_dose = snapshot['drug_dose'] if snapshot is not None else float('inf')
        self._min_drug_injs = float('inf')
        self._min_duration = float('inf')
        self._day_start = day_start
        self._duration = duration
        self._max_sequence_no_actions = max_sequence_no_actions
        self._time_slot = time_slot
        self._below_threshold = below_threshold
        self._horizon = self._day_start[1] + below_threshold + duration
        self._initstates = initstates
        self._species_thresholds = species_thresholds
        self._drugs = drugs
        setget = False
        if setget:
            self._paeon = [simulator.Modelica('Model', model_filenames, 0.5, getset=setget, initstate=i) for i in initstates] 
        else:
            self._paeon = [simulator.Modelica('Model', model_filenames, 0.5)]
        self._solver = backtrackingsolver.Solver(self._horizon, shuffle=random_ordering)
        self._solver += NoAction

        for d in drugs:
            for dose_idx in range(d.num_doses()):
                self._solver += ClinicalAction(d, dose_idx)

        self._start_actions = None
        if snapshot is not None:
            self._start_actions = []
            drug_list = snapshot['actions']
            for drug_dose in drug_list:
                if drug_dose == 'Nop':
                    self._start_actions.append(NoAction)
                elif drug_dose == 0.05:
                    self._start_actions.append(ClinicalAction(drugs[0], 0))
                elif drug_dose == 0.1:
                    self._start_actions.append(ClinicalAction(drugs[0], 1))
        self._start_stats = None
        if snapshot is not None:
            self._start_stats = backtrackingsolver.stats.Stats(nodes=snapshot['nodes'], solving_time=snapshot['solving_time'], sol=snapshot['solutions'])

        # duration_constraint = downregulation.Duration(
        #     self._day_start, self._duration, self._time_slot,
        # self._below_threshold)
        # self._solver += duration_constraint
        self._downregulation = Downregulation(self._initstates, self._day_start,
                                       self._below_threshold, self._duration,
                                       self._max_sequence_no_actions,
                                       self._time_slot,
                                       self._species_thresholds, self._paeon)
        alpha_n = alpha / float(drugs[0].dose(drugs[0].num_doses()-1) * (below_threshold + duration))
        beta_n = beta / float(below_threshold + duration)
        gamma_n = gamma / float(below_threshold + duration)
        # self._optimality = Optimality(alpha_n, beta_n, gamma_n, self._downregulation.treatment_duration, self._get_min_value)
        self._min_dose = MinDose(self._get_min_value, self._downregulation.treatment_duration)

        self._solver += self._min_dose
        self._solver += self._downregulation
        # self._solver += self._optimality
        self._outdir = outdir

    def getLogger(self):
        return self._logger

    def _get_min_value(self):
        return self._min_value

    def _start_search(self):
        if self._start_actions is not None and self._start_stats is not None:
            self._solver.start(from_actions=self._start_actions, stats=self._start_stats, L_start=0)
        else:
            self._solver.start()

    def stats(self):
        return self._solver.stats()

    def run(self):
        self._start_search()
        opt_treatment = None
        while True:
            _opt_treatment = self._next_solution()
            self._logger.info(self._solver.stats())
            if _opt_treatment is None:
                break
            else:
                opt_treatment = _opt_treatment
            self.store_treatment(opt_treatment, self._species_thresholds.keys())
        return opt_treatment, self._get_min_value()

    def _next_solution(self):
        sol = self._solver.solve()
        treatment = None
        if sol is not None:
            treatment = Treatment()
            overall_drug_dose = 0.0
            num_injs = 0
            for i, action in enumerate(sol):
                if action != NoAction:
                    overall_drug_dose += action.dose()
                    num_injs += 1
                    treatment += Injection(i * self._time_slot,
                                           action.dose_idx(), action.drug())
            # self._min_overall_drug_dose = overall_drug_dose
            # if self._downregulation.treatment_duration < self._min_duration:
            #     self._min_duration = self._downregulation.treatment_duration
            self._logger.info('New optimal treatment found:\n{},\ndrug dose: {},\nnum of injections {}'.format(treatment, overall_drug_dose, num_injs))
            self._min_value = overall_drug_dose
        return treatment

    def store_treatment(self, treatment, species, ):
        for initstate in self._initstates:
            start_time = initstate.transient_period()
            sol = self._paeon.simulate(initstate=initstate, start_time=start_time, final_time=len(treatment.injections()) + 60)
            sol.store('{}/solution_{}-{}.csv'.format(self._outdir, initstate.vp_id()[0], initstate.vp_id()[1]), species)
            sol = self._paeon.simulate(initstate=initstate, treatment=treatment, start_time=start_time, final_time=len(treatment.injections()) + 60)
            sol.store('{}/treatment_solution_{}-{}.csv'.format(self._outdir, initstate.vp_id()[0], initstate.vp_id()[1]), species)
            treatment.store('{}/treatment_{}-{}.csv'.format(self._outdir, initstate.vp_id()[0], initstate.vp_id()[1]))

