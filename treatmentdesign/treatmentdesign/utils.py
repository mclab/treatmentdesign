import math
import logging


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return not (math.isinf(a) or math.isinf(b)) and (abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol))
