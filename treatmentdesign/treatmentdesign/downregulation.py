import time
import math
import sys
import logging
try:
    from itertools import zip_longest as zip_longest
except:
    from itertools import izip_longest as zip_longest

from backtrackingsolver.constraint import Constraint
from backtrackingsolver.errors import InitializationError
from paeon.treatment.treatment import Treatment
from paeon.treatment.treatment import Injection

from .clinicalaction import NoAction
from . import utils


def check_safety_thresholds(sol, start, end, species_thresholds):
    num_timepoints = sol.num_timepoints()
    for i in range(num_timepoints):
        for species, threshold in species_thresholds.items():
            (tp, v) = sol.value(species, i)
            if tp < start or tp > end:
                continue
            if math.isnan(v) or (v > threshold and not utils.isclose(v, threshold, rel_tol=1e-6)):
                return False
    return True


class Downregulation(Constraint):
    def __init__(self, initstates, day_start, max_day_below_threshold, duration, max_sequence_no_actions,
                 time_slot, species_thresholds, paeon, num_processes=1, setget=False):
        self._name = 'Downregulation'
        self._logger = logging.getLogger(
            'treatmentdesign.downregulation.{}'.format(self._name))
        self._setget = setget
        self._paeon = paeon[0] if not setget else paeon
        if self._setget:
            self._states = [{} for i in initstates]
        self._initstates = initstates
        self._day_start = day_start
        self._duration = duration
        self._time_slot = time_slot
        self._depth_duration = int(math.ceil(duration / self._time_slot))
        self._species_thresholds = species_thresholds
        self._max_day_below_threshold = max_day_below_threshold

        self.depth_start_interval = int(math.ceil(
            day_start[0] / self._time_slot)), int(math.ceil(day_start[1] / self._time_slot))
        self._depth_first_injection = float('inf')

        self._day_below_threshold = -1
        self._depth_below_threshold = -1

        self._max_sequence_no_actions = max_sequence_no_actions
        self._max_sequence_no_actions_per_depth = [0]

    def __depth_duration(self):
        return self._depth_below_threshold + self._depth_duration

    def initialize(self, actions):
        self._depth_first_injection = float('inf')
        self._day_below_threshold = -1
        self._depth_below_threshold = -1
        self._max_sequence_no_actions_per_depth = [0]

        sat = True
        for idx in range(1, len(actions) + 1):
            sat = sat and self.check(actions[:idx])
        if not sat:
            raise InitializationError()

    def treatment_duration(self, actions):
        if self._depth_below_threshold < 0:
            return -1
        return self.__depth_duration() * self._time_slot

    def __check_safety_thresholds(self, initstate, sol, depth):
        start = self._depth_below_threshold * self._time_slot
        end = start + self._depth_duration
        self._logger.info('Check safety constraint from {} to {} for initstate {}.'.format(
            start, end, initstate.vp_id()))
        return check_safety_thresholds(sol, start, end, self._species_thresholds)

    def __simulate_treatment(self, initstate, treatment, actions, initstate_idx):
        sol = None
        final_time = len(actions) * self._time_slot
        # try:
        if self._setget:
            if '{}'.format(actions[:-1]) in self._states[initstate_idx]:
                state = self._states[initstate_idx]['{}'.format(actions[:-1])]
                self._paeon[initstate_idx].set_state(state)
                sol = self._paeon[initstate_idx].run((len(actions) * self._time_slot + initstate.transient_period()) - state['time'], treatment, initstate.transient_period())
            else:
                sol = self._paeon[initstate_idx].simulate(initstate=initstate, treatment=treatment,
                                   start_time=initstate.transient_period(), final_time=final_time)
        else:
            sol = self._paeon.simulate(initstate=initstate, treatment=treatment,
                       start_time=initstate.transient_period(), final_time=final_time)
        return sol
        # except Exception, e:
            # print(e)
            # return None

    def __depth_max_below_threshold(self):
        return self._depth_first_injection + int(math.ceil(self._max_day_below_threshold / self._time_slot))

    def __day_below_threshold(self, sol):
        for tp_idx, tp in enumerate(sol.timepoints()):
            if tp < self._depth_first_injection * self._time_slot:
                continue
            if tp > self.__depth_max_below_threshold() * self._time_slot:
                break
            is_below = True
            for species, threshold in self._species_thresholds.items():
                v = sol.value(species, tp_idx)[1]
                is_below = is_below and not math.isnan(v) and v <= threshold
            if is_below:
                return tp
        return -1

    def __check_start_conditions(self, actions):
        d = len(actions) - 1
        a = actions[-1]
        if d <= self._depth_first_injection:
            self._depth_first_injection = float('inf')
        if d < self.depth_start_interval[0] and a != NoAction:
            self._logger.debug('d {} < self.depth_start_interval[0] {} and a != NoAction {}'.format(
                d, self.depth_start_interval[0], a != NoAction))
            return False
        if d >= self.depth_start_interval[0] and d <= self.depth_start_interval[1] and d < self._depth_first_injection and a != NoAction:
            self._depth_first_injection = d
            self._logger.debug(
                'assign self._depth_first_injection to {}'.format(d))
        if d == self.depth_start_interval[1] and d < self._depth_first_injection and a == NoAction:
            self._logger.debug('d {} == self.depth_start_interval[1] {} and d < self._depth_first_injection {} and a == NoAction {}'.format(
                d, self.depth_start_interval[1], self._depth_first_injection, a == NoAction))
            return False
        return True

    def __check_end_conditions(self, actions):
        depth = len(actions) - 1
        if self._depth_below_threshold > 0 and depth > self.__depth_duration() and actions[-1] != NoAction:
            return False
        return True

    def __check_sequence_of_injections(self, actions):
        depth = len(actions) - 1
        a = actions[-1]
        seq_no_actions_len = 1
        if depth < len(self._max_sequence_no_actions_per_depth):
            self._max_sequence_no_actions_per_depth = self._max_sequence_no_actions_per_depth[:max(
                1, depth)]
        seq_no_actions_len += self._max_sequence_no_actions_per_depth[-1]
        if a != NoAction:
            seq_no_actions_len = 0
        self._max_sequence_no_actions_per_depth.append(seq_no_actions_len)
        if seq_no_actions_len > self._max_sequence_no_actions and depth > self._depth_first_injection:
            return False
        return True

    def __rotate(self, l, pivot):
        return l[pivot:] + l[:pivot]

    def check(self, actions):
        depth = len(actions) - 1
        # try:
        # print(actions[self._depth_first_injection:], self._depth_first_injection, actions[self._depth_first_injection], depth)
        # except:
        # pass
        sat = self.__check_start_conditions(actions)
        if not sat:
            self._logger.debug('Start conditions not satisfied')
            return sat
        sat = self.__check_end_conditions(actions)
        if not sat:
            self._logger.debug('End conditions not satisfied')
            return sat
        sat = self.__check_sequence_of_injections(actions)
        if not sat:
            self._logger.debug('Seq_NoActions not satisfied')
            return sat
        treatment = Treatment()
        for t, action in enumerate(actions):
            if action != NoAction:
                treatment += Injection(t * self._time_slot,
                                       action.dose_idx(), action.drug())
        if not treatment.injections():
            return True

        simulation_time = 0
        solutions = []
        if depth < self.__depth_max_below_threshold():
            self._day_below_threshold = -1
            self._depth_below_threshold = -1
        if depth == self.__depth_max_below_threshold():
            self._day_below_threshold = -1
            for initstate_idx, initstate in enumerate(self._initstates):
                t = time.time()
                sol = self.__simulate_treatment(
                    initstate, treatment, actions, initstate_idx)
                if sol is None:
                    self._initstates = self.__rotate(
                        self._initstates, initstate_idx)
                    if self._setget:
                        self._paeon = self.__rotate(self._paeon, initstate_idx)
                    return False
                if self._setget:
                    self._states[initstate_idx]['{}'.format(actions)] = self._paeon[initstate_idx].get_state()
                solutions.append(sol)
                simulation_time += time.time() - t
                day_below_threshold = self.__day_below_threshold(sol)
                if day_below_threshold == -1:
                    self._initstates = self.__rotate(
                        self._initstates, initstate_idx)
                    return False
                elif day_below_threshold > self._day_below_threshold:
                    self._day_below_threshold = day_below_threshold
                    self._depth_below_threshold = int(
                        math.ceil(self._day_below_threshold / self._time_slot))

        safety_time = 0
        if self._depth_below_threshold >= self._depth_first_injection:
            for initstate_idx, (initstate, solution) in enumerate(zip_longest(self._initstates, solutions)):
                if solution is None:
                    t = time.time()
                    sol = self.__simulate_treatment(initstate, treatment, actions, initstate_idx)
                    if sol is None:
                        self._initstates = self.__rotate(
                            self._initstates, initstate_idx)
                        return False
                    simulation_time += time.time() - t
                t = time.time()
                sat = self.__check_safety_thresholds(initstate, sol, depth)
                safety_time += time.time() - t
                if not sat:
                    # print('Safety conditions not satisfied')
                    self._initstates = self.__rotate(
                        self._initstates, initstate_idx)
                    return False
        self._logger.info(
            'Time spent in simulating virtual patients {:.3f} (s)'.format(simulation_time))
        self._logger.info(
            'Time spent in checking safety conditions {:.3f} (s)'.format(safety_time))
        return True
