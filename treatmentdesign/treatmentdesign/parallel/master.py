import pickle
from mpi4py import MPI
import time
import logging

from .. import utils
from backtrackingsolver.stats import Stats
from treatmentdesign import TreatmentDesign
from .tags import Tags


class Master(TreatmentDesign):
    def __init__(self, day_start, duration, time_slot, below_threshold, max_sequence_no_actions,
                 initstates, species_thresholds, drugs, model_filenames, alpha, beta, gamma, comm,
                 L, perc_waiting_time, snapshot_file=None, random_ordering=False, outdir='.'):
        super(Master, self).__init__(
            day_start, duration, time_slot, below_threshold, max_sequence_no_actions, initstates,
            species_thresholds, drugs, model_filenames, alpha, beta, gamma, random_ordering=random_ordering, outdir=outdir)
        self._logger = logging.getLogger('treatmentdesign.TreatmentDesign.Master')
        self._comm = comm
        self._status = MPI.Status()
        self._slaves = self._comm.Get_size() - 1
        self._L = L
        self._perc_waiting_time = perc_waiting_time
        self._time = None
        self._optimal_treatment = None
        self.stats = Stats()
        self._snapshot_file = snapshot_file

    def __bcast(self, data, tag):
        for s in range(1, self._slaves + 1):
            self._comm.send(data, dest=s, tag=tag)
            self._logger.debug('Send {} to slave {} with tag {}'.format(data, s, tag))

    def __ibcast(self, data, tag):
        for s in range(1, self._slaves + 1):
            self._comm.isend(data, dest=s, tag=tag)

    def _start_search(self):
        self.__bcast(None, Tags.START)
        self._solver.start(L=self._L)

    def __start_timer(self):
        self._time = time.time()

    def __stop_timer(self):
        if self._time is not None:
            start = self._time
            self._time = None
            return time.time() - start
        return -1

    def __update_and_notify(self, treatment, value):
        if value < self._min_value:
            self._optimal_treatment = treatment
            self._min_value = value 
            self._logger.info("Im notifying slaves about new updates {}".format(self._min_value))
            self.__bcast(self._min_value, Tags.NOTIFICATION)
            return True
        return False

    def __snapshot(self, jobs, waiting_time):
        solver_state = self._solver.getstate()
        with open('{}/snapshot.dat'.format(self._outdir), 'w+') as f:
            pickle.dump([[v for _,v in jobs.items() if v is not None], waiting_time, solver_state], f)

    def __recover(self, snapshot_file):
        with open(snapshot_file) as f:
            data = pickle.load(f)
            self._logger.debug(data)
            jobs = data[0]
            self._logger.debug(jobs)
            waiting_time = data[1]
            self._logger.debug(waiting_time)
            self._solver.setstate(data[2])
            return jobs, waiting_time

    def run(self):
        self._logger.info('Hello, Im the Master!')
        self._start_search()
        workers = self._slaves
        prev_waiting_time = None
        done = False
        jobs = {slave: None for slave in range(1,workers+1)}
        recover_jobs = []
        if self._snapshot_file is not None:
            recover_jobs, prev_waiting_time = self.__recover(self._snapshot_file)
        while workers > 0:
            self._logger.info('Optimal treatment found so far: {} with value: {} and stats:\n{}'.format(self._optimal_treatment, self._min_value, self.stats))
            data = self._comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=self._status)
            tag = self._status.Get_tag()
            slave = self._status.Get_source()
            if tag == Tags.UPDATE:
                treatment, value = pickle.loads(data)
                self._logger.info('I received an optimal treatment {} with value {} from slave {}'.format(treatment, value, slave))
                self.__update_and_notify(treatment, value)
            elif tag == Tags.READY:
                if data is not None:
                    stats = pickle.loads(data)
                    self._logger.info('I received the following stats from slave {}.\n{}'.format(slave, stats))
                    self.stats += stats
                    jobs[slave] = None
                partial_sol = None
                if not done:
                    waiting_time = self.__stop_timer()
                    if not recover_jobs:
                        if prev_waiting_time is not None:
                            if waiting_time > prev_waiting_time * (1.0 + self._perc_waiting_time[1]):
                                self._solver.incr_search_layer()
                                self._logger.info('Im waiting too much, I will help slaves')
                            elif waiting_time < prev_waiting_time * (1.0 - self._perc_waiting_time[0]):
                                self._solver.decr_search_layer()
                                self._logger.info('Im working too much, slaves will help me')
                        prev_waiting_time = waiting_time
                        partial_sol = self._next_solution()
                    else:
                        partial_sol = recover_jobs.pop()
                        if not recover_jobs:
                            recover_jobs = None
                if partial_sol is not None:
                    self._logger.info('Send {} to slave {}'.format(partial_sol, slave))
                    self._comm.send(pickle.dumps(partial_sol), dest=slave, tag=Tags.WORK)
                    jobs[slave] = partial_sol
                    self.__snapshot(jobs, prev_waiting_time)
                    self.__start_timer()
                else:
                    done = True
                    self._logger.info('No work to do')
                    self._comm.send(None, dest=slave, tag=Tags.FINISH)
            elif tag == Tags.FINISH:
                workers -= 1
                self._logger.info(
                    'Thank you for your work, slave {}. Now there are {} worker(s) left.'.
                    format(slave, workers))
            else:
                self._logger.error('Smt bad: slave {} tag {}'.format(slave, tag))
                break
        self.__bcast(None, Tags.EXIT)
        self.stats += self._solver.stats()
        return self._optimal_treatment, self._get_min_value()

    def _next_solution(self):
        return self._solver.solve()
