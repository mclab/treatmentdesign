import pickle
import logging
import time
from mpi4py import MPI

from .. import utils
from ..treatmentdesign import TreatmentDesign
from .tags import Tags

class Slave(TreatmentDesign):
    def __init__(self, day_start, duration, time_slot, below_threshold, max_sequence_no_actions,
                 initstates, species_thresholds, drugs, model_filenames, alpha, beta, gamma, comm, random_ordering=False, outdir='.'):
        super(Slave, self).__init__(
            day_start, duration, time_slot, below_threshold, max_sequence_no_actions, initstates,
            species_thresholds, drugs, model_filenames, alpha, beta, gamma, random_ordering=random_ordering, outdir=outdir)
        self._comm = comm
        self._myrank = self._comm.Get_rank() 
        self._status = MPI.Status()
        self._time = None
        self._logger = logging.getLogger('treatmentdesign.TreatmentDesign.Slave-{}'.format(self._myrank))

    def __start_search(self, actions):
        self._solver.start(from_actions=actions)

    def __wait_for_update(self):
        completed = self._comm.iprobe(source=0, tag=Tags.NOTIFICATION)
        if completed:
            value = self._comm.recv(source=0, tag=Tags.NOTIFICATION)
            self._logger.info('I received updates from master: {}'.format(value))
            if value < self._min_value:
                self._min_value = value 

    def _get_min_value(self):
        # if self._time is None or self._time < time.time() - 10:
        self.__wait_for_update()
            # self._time = time.time()
        return self._min_value

    def run(self):
        while True:
            data = self._comm.recv(source=0, tag=MPI.ANY_TAG, status=self._status)
            tag = self._status.Get_tag()
            if tag == Tags.START:
                self._logger.info('Hi, Im slave no. {}. It is time to start.'.format(self._myrank))
                self._comm.send(None, dest=0, tag=Tags.READY)
            elif tag == Tags.NOTIFICATION:
                self._logger.info('I received updates from master: {}'.format(data))
                if data < self._min_value:
                    self._min_value = data
            elif tag == Tags.WORK:
                actions = pickle.loads(data)
                self._logger.info('Work received {}'.format(actions))
                try:
                    self.__start_search(actions)
                    while True:
                        _opt_treatment = self._next_solution()
                        if _opt_treatment is None:
                            break
                        self._logger.info('Im sending the treatment {} with value {} to the master.'.format(_opt_treatment, self._get_min_value()))
                        self._comm.send(pickle.dumps([_opt_treatment, self._min_value]), dest=0, tag=Tags.UPDATE)
                    stats = self._solver.stats()
                    self._logger.info('Finished current work with the following stats.\n{}'.format(stats))
                except InitializationError, e:
                    self._logger.error('Initialization error raised by the current work.')
                    pass
                self._comm.send(pickle.dumps(stats), dest=0, tag=Tags.READY)
            elif tag == Tags.FINISH:
                self._logger.info('I finished to work.')
                self._comm.send(None, dest=0, tag=Tags.FINISH)
            elif tag == Tags.EXIT:
                self._logger.info('See you.')
                break
            else:
                self._logger.warning('Unexpected tag')
                break
