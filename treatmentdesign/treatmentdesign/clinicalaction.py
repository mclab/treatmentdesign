from backtrackingsolver.action import Action
from paeon.treatment.drug import Drug

class ClinicalAction(Action):

    def __init__(self, drug, dose_idx):
        self._name = drug.name()
        self._drug = drug
        self._dose_idx = dose_idx

    def drug(self):
        return self._drug

    def dose_idx(self):
        return self._dose_idx

    def dose(self):
        return self._drug.dose(self._dose_idx)

    def __eq__(self, other):
        if isinstance(other, ClinicalAction):
            return self._name == other._name and self._drug == other._drug and self._dose_idx == other._dose_idx
        return False

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return '<{}, {}>'.format(self._name, self.dose())
    __repr__ = __str__

NoAction = Action('Nop')
