model Model
  GynCycle.DefV2 patientData;
  GynCycle.Administrations.InjectionsInput decapeptyl;
  GynCycle.Model gyncycle(patientData = patientData,
                                       decapeptyl = decapeptyl
                                       );
equation
end Model;

