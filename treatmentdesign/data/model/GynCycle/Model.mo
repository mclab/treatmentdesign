within GynCycle;
block Model
  extends Interface;

  DefV2 patientData;

  Administrations.InjectionsInput decapeptyl;

  Administrations.Single decapeptyl_adm(drug = Administrations.Drugs.Decapeptyl, injections = decapeptyl);

  constant Real InitialValues[SpeciesNum] = {
    259696,
    2.71,
    8.398,
    0.266,
    0.71,
    59734,
    5.28,
    5.903,
    0.796,
    1.8,
    0.156,
    2.593,
    23.14,
    0.492,
    0.0000161,
    0.2449,
    0,
    0.00000001,
    0.000002,
    0.00002,
    0.0003,
    0.003,
    0.011,
    41.36,
    1.978,
    0.93,
    60.53,
    71.14,
    11.15,
    0.0012,
    0.0152,
    0.009,
    0.00096,
    0.000065,
    0.000059
  };

  parameter Real p[LambdaSize] = {
    7309.92,
    7309.92,
    0.00476,
    0.1904,
    5,
    2.143,
    74.851,
    68.949,
    183.36,
    22130,
    0.057,
    0.272,
    3.529,
    114.25,
    61.029,
    138.3,
    0.219,
    1.343,
    3.663,
    1.221,
    4.882,
    2.726,
    0.122,
    10,
    122.06,
    12.206,
    332.75,
    122.06,
    7.984,
    12.206,
    1.208,
    1.221,
    0.958,
    0.925,
    20,
    0.7567,
    0.61,
    0.543,
    51.558,
    2.0945,
    9.28,
    3480.27,
    0.972,
    1713.71,
    8675.14,
    5.235,
    0.943,
    761.64,
    5.13,
    1.445,
    2.285,
    60,
    180,
    28.211,
    194.07,
    114.25,
    4.287,
    89.943,
    447.47,
    132240.2,
    172.45,
    0.199,
    16,
    1,
    0.005593,
    322.18,
    644.35,
    0.447,
    3.222,
    32.218,
    0.00008949,
    32.218,
    0.0895,
    32.218,
    3.222,
    0.00895
  };

  parameter Real P1_1 = patientData.P1_1 "basal LH synthesis rate parameter";
  parameter Real P1_2 = patientData.P1_2 "E2 promoted LH synthesis rate parameter";
  parameter Real P1_3 = 192.2 "threshold of E2 on LH synthesis";
  parameter Real P1_4 = 10 "Hill exponent";
  parameter Real P1_5 = 2.371 "threshold of P4 on LH synthesis";
  parameter Real P1_6 = 1 "Hill exponent";
  parameter Real P1_7 = patientData.P1_7 "basal LH release rate parameter";
  parameter Real P1_8 = patientData.P1_8 "G_R_a promoted LH release rate parameter";
  parameter Real P1_9 = 0.0003 "threshold of G_R_a on LH release";
  parameter Real P1_10 = 5 "Hill exponent";
  parameter Real P2_1 = patientData.P2_1 "blood volume";
  parameter Real P2_2 = patientData.P2_2 "binding rate of LH to its receptor";
  parameter Real P2_3 = patientData.P2_3 "clearance rate of LH from the blood";
  parameter Real P3_1 = patientData.P3_1 "formation rate of free LH receptors";
  parameter Real P4_1 = patientData.P4_1 "desensitization rate of LH receptor complex";
  parameter Real P6_1 = patientData.P6_1 "basal FSH synthesis rate parameter";
  parameter Real P6_2 = 95.81 "threshold of IhA_e in FSH synthesis";
  parameter Real P6_3 = 5 "Hill exponent";
  parameter Real P6_4 = 70 "threshold of IhB in FSH synthesis";
  parameter Real P6_5 = 3 "Hill exponent";
  parameter Real P6_6 = 10 "threshold of GnRH frequency in FSH synthesis";
  parameter Real P6_7 = 3 "Hill exponent";
  parameter Real P6_8 = patientData.P6_8 "basal FSH release rate parameter";
  parameter Real P6_9 = patientData.P6_9 "stimulation of FSH release by GnRH rec";
  parameter Real P6_10 = 0.0003 "threshold of GnRH on FSH release";
  parameter Real P6_11 = 3 "Hill exponent";
  parameter Real P7_1 = patientData.P7_1 "binding rate of FSH to its receptor";
  parameter Real P7_2 = patientData.P7_2 "clearance rate of FSH from blood";
  parameter Real P8_1 = patientData.P8_1 "formation rate of free FSH receptors";
  parameter Real P9_1 = patientData.P9_1 "desenzitation rate of FSH receptos complex";
  parameter Real P11_1 = patientData.P11_1 "synth. rate const. of FSH_blood to s";
  parameter Real P11_2 = 3 "threshold of FSH_blood to stimulate s";
  parameter Real P11_3 = 5 "Hill exponent";
  parameter Real P11_4 = patientData.P11_4 "s clearance rate parameter";
  parameter Real P11_5 = 1.235 "threshold of P4 on clearance of s";
  parameter Real P11_6 = 5 "Hill exponent";
  parameter Real P12_1 = patientData.P12_1 "growth rate of AF1";
  parameter Real P12_2 = 0.608 "threshold of FSH_R for stimul. of AF1";
  parameter Real P12_3 = 3 "Hill exponent";
  parameter Real P12_4 = patientData.P12_4 "transition rate parameter from AF2 to AF2";
  parameter Real P13_1 = patientData.P13_1 "transition rate parameter from AF2 to AF3";
  parameter Real P13_2 = patientData.P13_2 "scaling of LH receptor complex";
  parameter Real P13_3 = 3.689 "Hill exponent";
  parameter Real P14_1 = patientData.P14_1 "self growth rate of AF3";
  parameter Real P14_2 = patientData.P14_2 "maximum size of AF3 and AF4";
  parameter Real P14_3 = patientData.P14_3 "transition rate parameter from AF3 to AF4";
  parameter Real P14_4 = 5 "Hill exponent";
  parameter Real P15_1 = patientData.P15_1 "self growth rate of AF4";
  parameter Real P15_2 = 2 "Hill exponent";
  parameter Real P15_3 = patientData.P15_3 "transition rate parameter from AF4 to PrF";
  parameter Real P16_1 = patientData.P16_1 "elimination rate of PrF";
  parameter Real P16_2 = 6 "Hill exponent";
  parameter Real P17_1 = patientData.P17_1 "rowth rate of OvF";
  parameter Real P17_2 = 3 "hreshold of PrF for OvF formation";
  parameter Real P17_3 = 10 "Hill exponent";
  parameter Real P17_4 = patientData.P17_4 "elimination rate parameter of OvF";
  parameter Real P18_1 = patientData.P18_1 "growth rate of Sc1 stimulated by OvF";
  parameter Real P18_2 = 0.02 "threshold of OvF to form Sc1";
  parameter Real P18_3 = 10 "Hill exponent";
  parameter Real P18_4 = patientData.P18_4 "transition rate parameter from Sc1 to Sc2";
  parameter Real P19_1 = patientData.P19_1 "transition rate from Sc2 to Lut1";
  parameter Real P20_1 = patientData.P20_1 "transition rate parameter from Lut1 to Lut2";
  parameter Real P20_2 = patientData.P20_2 "rate of G_R_a to the luteal development";
  parameter Real P20_3 = 0.0008 "threshold of G_R_a to the luteal development";
  parameter Real P20_4 = 5 "Hill exponent";
  parameter Real P21_1 = patientData.P21_1 "transition rate parameter from Lut2 to Lut3";
  parameter Real P22_1 = patientData.P22_1 "transition rate parameter from Lut3 to Lut4a";
  parameter Real P23_1 = patientData.P23_1 "clearance rate parameter of Lut4";
  parameter Real P24_1 = patientData.P24_1 "basal E2 production";
  parameter Real P24_2 = patientData.P24_2 "production of E2 by AF2";
  parameter Real P24_3 = patientData.P24_3 "production of E2 by LH and AF3";
  parameter Real P24_4 = patientData.P24_4 "production of E2 by AF4";
  parameter Real P24_5 = patientData.P24_5 "production of E2 by LH and PrF";
  parameter Real P24_6 = patientData.P24_6 "production of E2 by Lut1";
  parameter Real P24_7 = patientData.P24_7 "production of E2 by Lut4";
  parameter Real P24_8 = patientData.P24_8 "E2 clearance rate parameter";
  parameter Real P25_1 = patientData.P25_1 "basal P4 production";
  parameter Real P25_2 = patientData.P25_2 "production of P4 by Lut4";
  parameter Real P25_3 = patientData.P25_3 "P4 clearance rate parameter";
  parameter Real P26_1 = patientData.P26_1 "basal IhA production";
  parameter Real P26_2 = patientData.P26_2 "production of IhA by PrF";
  parameter Real P26_3 = patientData.P26_3 "production of IhA by Sc1";
  parameter Real P26_4 = patientData.P26_4 "production of IhA by Lut1";
  parameter Real P26_5 = patientData.P26_5 "production of IhA by Lut2";
  parameter Real P26_6 = patientData.P26_6 "production of IhA by Lut3";
  parameter Real P26_7 = patientData.P26_7 "production of IhA by Lut4";
  parameter Real P26_8 = patientData.P26_8 "IhA clearance rate parameter";
  parameter Real P27_1 = patientData.P27_1 "basal IhB production";
  parameter Real P27_2 = patientData.P27_2 "production of IhB by AF2";
  parameter Real P27_3 = patientData.P27_3 "production of IhB by AF3";
  parameter Real P27_4 = patientData.P27_4 "IhB clearance rate parameter";
  parameter Real P28_1 = patientData.P28_1 "clearance of IhA in delayed compartment";
  parameter Real P29_1 = patientData.P29_1 "mean GnRH frequency";
  parameter Real P29_2 = 3 "threshold of P4 for inhibition of GnRH frequency";
  parameter Real P29_3 = 2 "Hill exponent";
  parameter Real P29_4 = patientData.P29_4 "factor for stimulation by E2";
  parameter Real P29_5 = 220 "threshold of E2 for stimulation of GnRH frequency";
  parameter Real P29_6 = 10 "Hill exponent";
  parameter Real P30_1 = patientData.P30_1 "amount of GnRH released by one puls at heigh E2 level";
  parameter Real P30_2 = 220 "threshold of E2 for stimulation of GnRH mass";
  parameter Real P30_3 = 2 "Hill exponent";
  parameter Real P30_4 = 9.6 "threshold of E2 for inhibition of GnRH mass";
  parameter Real P30_5 = 1 "Hill exponent";
  parameter Real P31_1 = patientData.P31_1 "binding rate of GnRH to its receptor";
  parameter Real P31_2 = patientData.P31_2 "breakup rate of GnRH-receptor complex";
  parameter Real P31_3 = patientData.P31_3 "degradation rate of GnRH";
  parameter Real P32_1 = patientData.P32_1 "rate of receptor inactivation";
  parameter Real P32_2 = patientData.P32_2 "rate of receptor activation";
  parameter Real P33_1 = patientData.P33_1 "synthesis rate of inactive receptors";
  parameter Real P33_2 = patientData.P33_2 "dissociation rate of inactive receptor complex";
  parameter Real P33_3 = patientData.P33_3 "degradation rate of inactive receptors";
  parameter Real P34_1 = patientData.P34_1 "rate of receptor complex inactivation";
  parameter Real P34_2 = patientData.P34_2 "rate of receptor complex activation";
  parameter Real P35_1 = patientData.P35_1 "degradation rate of inactive receptor complex";

  Real z_P4;
  Real a_freq;
  Real a_mass;
  Real syn_LH;
  Real rel_LH;
  Real syn_FSH;
  Real rel_FSH;

initial equation
  for i in 1:SpeciesNum loop
    species[i] = InitialValues[i];
  end for;

algorithm
  z_P4 := species[25];
  a_freq := P29_1 * HMinus(z_P4, P29_2, P29_3) * (1.0 + P29_4 * HPlus(species[24], P29_5, P29_6));
  a_mass := P30_1 * (HPlus(species[24], P30_2, P30_3) + HMinus(species[24], P30_4, P30_5));
  syn_LH := (P1_1 + P1_2 * HPlus(species[24], P1_3, P1_4)) * HMinus(z_P4, P1_5, P1_6);
  rel_LH := P1_7 + P1_8 * HPlus(species[34], P1_9, P1_10);
  syn_FSH := P6_1 / (1.0 + (species[28] / P6_2) ^ P6_3 + (species[27] / P6_4) ^ P6_5) * HMinus(a_freq, P6_6, P6_7);
  rel_FSH := P6_8 + P6_9 * HPlus(species[34], P6_10, P6_11);

equation

  der(species[1]) = syn_LH - rel_LH * species[1] "1";
  der(species[2]) = rel_LH * species[1] / P2_1 - (P2_2 * species[3] + P2_3) * species[2] "2";
  der(species[3]) = P3_1 * species[5] - P2_2 * species[2] * species[3] "3";
  der(species[4]) = P2_2 * species[2] * species[3] - P4_1 * species[4] "4";
  der(species[5]) = P4_1 * species[4] - P3_1 * species[5] "5";
  der(species[6]) = syn_FSH - rel_FSH * species[6] "6";
  der(species[7]) = rel_FSH * species[6] / P2_1 - (P7_1 * species[8] + P7_2) * species[7] "7";
  der(species[8]) = P8_1 * species[10] - P7_1 * species[7] * species[8] "8";
  der(species[9]) = P7_1 * species[7] * species[8] - P9_1 * species[9] "9";
  der(species[10]) = P9_1 * species[9] - P8_1 * species[10] "10";
  der(species[11]) = P11_1 * HPlus(species[7], P11_2, P11_3) - P11_4 * HPlus(z_P4, P11_5, P11_6) * species[11] "11";
  der(species[12]) = P12_1 * HPlus(species[9], P12_2, P12_3) - P12_4 * species[9] * species[12] "12";
  der(species[13]) = P12_4 * species[9] * species[12] - P13_1 * (species[4] / P13_2) ^ P13_3 * species[11] * species[13] "13";
  der(species[14]) = P13_1 * (species[4] / P13_2) ^ P13_3 * species[11] * species[13] + P14_1 * species[9] * species[14] * (1.0 - species[14] / P14_2) - P14_3 * (species[4] / P13_2) ^ P14_4 * species[11] * species[14] "14";
  der(species[15]) = P14_3 * (species[4] / P13_2) ^ P14_4 * species[11] * species[14] + P15_1 * (species[4] / P13_2) ^ P15_2 * species[15] * (1.0 - species[15] / P14_2) - P15_3 * (species[4] / P13_2) * species[11] * species[15] "15";
  der(species[16]) = P15_3 * (species[4] / P13_2) * species[11] * species[15] - P16_1 * (species[4] / P13_2) ^ P16_2 * species[11] * species[16] "16";
  der(species[17]) = P17_1 * (species[4] / P13_2) ^ P16_2 * species[11] * HPlus(species[16], P17_2, P17_3) - P17_4 * species[17] "17";
  der(species[18]) = P18_1 * HPlus(species[17], P18_2, P18_3) - P18_4 * species[18] "18";
  der(species[19]) = P18_4 * species[18] - P19_1 * species[19] "19";
  der(species[20]) = P19_1 * species[19] - P20_1 * (1.0 + P20_2 * HPlus(species[34], P20_3, P20_4)) * species[20] "20";
  der(species[21]) = P20_1 * species[20] - P21_1 * (1.0 + P20_2 * HPlus(species[34], P20_3, P20_4)) * species[21] "21";
  der(species[22]) = P21_1 * species[21] - P22_1 * (1.0 + P20_2 * HPlus(species[34], P20_3, P20_4)) * species[22] "22";
  der(species[23]) = P22_1 * species[22] - P23_1 * (1.0 + P20_2 * HPlus(species[34], P20_3, P20_4)) * species[23] "23";
  der(species[24]) = P24_1 + P24_2 * species[13] + P24_3 * species[2] * species[14] + P24_4 * species[15] + P24_5 * species[2] * species[16] + P24_6 * species[20] + P24_7 * species[23] - P24_8 * species[24] "24";
  der(species[25]) = P25_1 + P25_2 * species[23] - P25_3 * species[25] "25";
  der(species[26]) = P26_1 + P26_2 * species[16] + P26_3 * species[18] + P26_4 * species[20] + P26_5 * species[21] + P26_6 * species[22] + P26_7 * species[23] - P26_8 * species[26] "26";
  der(species[27]) = P27_1 + P27_2 * species[13] + P27_3 * species[19] - P27_4 * species[27] "27";
  der(species[28]) = P26_8 * species[26] - P28_1 * species[28] "28";
  der(species[29]) = P29_1 * HMinus(z_P4, P29_2, P29_3) * (1.0 + P29_4 * HPlus(species[24], P29_5, P29_6)) - species[29] "29";
  der(species[30]) = P30_1 * (HPlus(species[24], P30_2, P30_3) + HMinus(species[24], P30_4, P30_5)) - species[30] "30";
  der(species[31]) = a_mass * a_freq - P31_1 * species[31] * species[32] + P31_2 * species[34] - P31_3 * species[31] + Administrations.Drugs.Decapeptyl.pk.clearanceRate * decapeptyl_adm.v / 40.65 "31";
  der(species[32]) = P31_2 * species[34] - P31_1 * species[31] * species[32] - P32_1 * species[32] + P32_2 * species[33] "32";
  der(species[33]) = P33_1 + P32_1 * species[32] - P32_2 * species[33] + P33_2 * species[35] - P33_3 * species[33] "33";
  der(species[34]) = P31_1 * species[31] * species[32] - P31_2 * species[34] - P34_1 * species[34] + P34_2 * species[35] "34";
  der(species[35]) = P34_1 * species[34] - P34_2 * species[35] - P33_2 * species[35] - P35_1 * species[35] "35";

end Model;
