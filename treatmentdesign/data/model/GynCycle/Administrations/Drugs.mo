within GynCycle.Administrations;
package Drugs
  constant Drug Decapeptyl (
    pk = PK (
      beta = 75.0,
      clearanceRate = 6.0
    ),
    doseMultiplier = 1
  );
  constant Drug Primolut (
    pk = PK (
      beta = 52.324,
      clearanceRate = 11.09
    ),
    doseMultiplier = 1
  );
end Drugs;
