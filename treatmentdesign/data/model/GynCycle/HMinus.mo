within GynCycle;
function HMinus
  input Real s;
  input Real t;
  input Real n;
  output Real result;
algorithm
  result := 1.0 / (1.0 + (s / t) ^ n);
end HMinus;
