from setuptools import setup



setup(name='paeon',
        version='1.0',
        description='Paeon',
        author='Stefano Sinisi',
        author_email='sinisi@di.uniroma1.it',
        packages=['paeon', 'paeon.treatment']
        )
