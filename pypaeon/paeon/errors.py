class CompilationError(Exception):
    pass

class InitStateParsingError(Exception):
    pass

class WrongTimepointIdxComputation(Exception):
    pass
