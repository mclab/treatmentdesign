from .drug import Drug
from .injection import Injection
from .treatment import Treatment

import logging
module_logger = logging.getLogger('paeon.treatment')
module_logger.addHandler(logging.NullHandler())
