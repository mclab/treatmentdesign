from .injection import Injection

class Treatment(object):

    def __init__(self):
        self._injections = []
        self._start = float('inf')
        self._end = float('-inf')

    def __iadd__(self, other):
        if isinstance(other, Injection):
            self._injections.append(other)
            timepoint = other.timepoint()
            if timepoint < self._start:
                self._start = timepoint
            if timepoint > self._end:
                self._end = timepoint
        else:
            raise TypeError("unsupported operand type(s) for += '{}' and '{}'".format(self.__class__, type(other)))
        return self

    def injections(self):
        return self._injections

    def remove_last(self):
        t = Treatment()
        t._injections = self._injections[:-1]
        t.start = self._start
        t.end = t._injections[-1].timepoint
        return t
    
    def clear(self):
        self = Treatment()

    def store(self, filename, timeshift=0.0):
        with open(filename, 'w') as f:
            for i, inj in enumerate(self.injections()):
                f.write('{}, {}, {}\n'.format(inj.timepoint() + timeshift, inj.dose(), inj.drug().name()))

    def __str__(self):
        return self._injections.__str__()

    __repr__ = __str__

