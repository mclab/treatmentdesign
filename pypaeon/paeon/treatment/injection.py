from .drug import Drug

class Injection(object):

    def __init__(self, timepoint, dose_idx, drug):
        self._tp = timepoint
        self._dose_idx = dose_idx
        self._drug = drug
        
    def timepoint(self):
        return self._tp

    def dose(self):
        return self._drug.dose(self._dose_idx)

    def drug(self):
        return self._drug.copy()

    def __str__(self):
        return '({}, {}, {})'.format(self._tp, self._drug.name(), self._drug.dose(self._dose_idx))
    __repr__ = __str__
