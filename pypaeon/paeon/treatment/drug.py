
class Drug(object):

    def __init__(self, name, descr, doses=[]):
        self._name = name
        self._descr = descr
        self._doses = doses

    def __iadd__(self, other):
        if isinstance(other, float):
            self._doses.append(other)
        else:
            raise TypeError("unsupported operand type(s) for += '{}' and '{}'".format(self.__class__, type(other)))
        return self
    
    def num_doses(self):
        return len(self._doses)

    def dose(self, idx):
        return self._doses[idx]

    def name(self):
        return self._name

    def copy(self):
        return Drug(self._name, self._descr, self._doses)

    def __eq__(self, other):
        if not isinstance(other, Drug):
            return False
        return self._name == other._name and self._descr == other._descr and self._doses == other._doses

    def __ne__(self, other):
        return not self == other

    def __str__(self):
        return '{} ({}): {}'.format(self._name, self._descr, self._doses.__str__())

    __repr__ = __str__
