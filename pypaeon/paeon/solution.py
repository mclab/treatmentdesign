import numpy as np
from scipy.interpolate import interp1d
import math
import gzip
import csv
import pprint
import logging

from collections import OrderedDict

from . import errors


class Measure(object):
    def __init__(self):
        self._result = float('inf')
        self._name = 'None'

    def name(self):
        return self._name

    def add(self, obs, prediction):
        pass

    def value(self):
        return self._result

    def __str__(self):
        return '{}'.format(self._result)
    __repr__ = __str__


class MAPE(Measure):
    def __init__(self):
        super(Measure, self).__init__()
        self._name = 'MAPE'
        self._num_obs = 0
        self._value = 0.0
        self._result = float('inf')

    def add(self, obs, prediction):
        self._value += abs(obs - prediction)/float(obs)
        self._num_obs += 1
        self._result = self._value / self._num_obs


class SMAPE(Measure):
    def __init__(self):
        super(Measure, self).__init__()
        self._name = 'SMAPE'
        self._num_obs = 0
        self._value = 0.0
        self._result = float('inf')

    def add(self, obs, prediction):
        self._value = abs((obs - prediction)/(obs + prediction))
        self._num_obs += 1
        self._result = self._value / self._num_obs


class MAE(Measure):
    def __init__(self):
        super(Measure, self).__init__()
        self._name = 'MAE'
        self._num_obs = 0
        self._value = 0.0
        self._result = float('inf')

    def add(self, obs, prediction):
        self._value += abs(obs - prediction)
        self._num_obs += 1
        self._result = self._value / self._num_obs


class CorrCoef(Measure):
    def __init__(self):
        super(Measure, self).__init__()
        self._name = 'CorrCoef'
        self._obs = []
        self._pred = []
        self._result = float('inf')

    def add(self, obs, prediction):
        self._obs.append(obs)
        self._pred.append(prediction)
        self._result = np.corrcoef(self._pred, self._obs)[0][1]


class Toni(Measure):
    def __init__(self):
        super(Measure, self).__init__()
        self._name = 'Toni'
        self._num = 0.0
        self._den = 0.0
        self._result = float('inf')

    def add(self, obs, prediction):
        self._num += abs(obs - prediction)
        self._den += obs
        self._result = self._num / self._den


class Evaluation(object):
    def __init__(self, species):
        self._species = species

    def evaluate(self, mc, sol, distance, shift=0):
        measures = { s: [CorrCoef(), MAPE(), SMAPE(), MAE(), Toni()] for s in self._species}
        aggregates = {'CorrCoef': 0.0, 'MAPE': 0.0, 'SMAPE': 0.0, 'MAE': 0.0, 'Toni': 0.0}
        used_species = []
        for s in self._species:
            used_species.append(s)
            mc_tps = []
            for tp_idx, tp in enumerate(mc.timepoints()):
                mc_tp, mc_value = mc.value(s, tp_idx)
                if math.isnan(mc_value):
                    continue
                sol_tp_idx = sol.timepoint_idx_at_time(mc_tp + shift)
                if sol_tp_idx is None:
                    continue
                mc_tps.append(mc_tp)
                _, sol_value = sol.value(s, sol_tp_idx)
                for m in measures[s]:
                    m.add(mc_value, sol_value)
            if not mc_tps:
                used_species.pop()
        for s in used_species:
            for m in measures[s]:
                aggregates[m.name()] += m.value()
        for name in aggregates.iterkeys():
            aggregates[name] /= len(used_species)
        return measures, aggregates

def align(sol, mc, species):
    mc_t_idx = mc.timepoint_idx_at_time(0)
    _, mc_value = mc.value(species, mc_t_idx)
    mc_t_pre, _ = mc.value(species, mc_t_idx - 1)
    mc_t_post, _ = mc.value(species, mc_t_idx + 1)
    try:
        sol_t_idx = sol.timepoint_idx_at_time(0)
        sol_t_idx_pre = sol.timepoint_idx_at_time(mc_t_pre)
        sol_t_idx_post = sol.timepoint_idx_at_time(mc_t_post)
    except errors.WrongTimepointIdxComputation:
        return []
    if sol_t_idx is None or sol_t_idx_pre is None or sol_t_idx_post is None:
        return []
    tau_list = []
    tau = 0
    diff = float('inf')
    for t_idx in range(sol_t_idx_pre, sol_t_idx + 1):
        t, v = sol.value(species, t_idx)
        if abs(v - mc_value) < diff:
            diff = abs(v - mc_value)
            tau = t
    tau_list.append(tau)
    tau = 0
    diff = float('inf')
    for t_idx in range(sol_t_idx + 1, sol_t_idx_post + 1):
        t, v = sol.value(species, t_idx)
        if abs(v - mc_value) < diff:
            diff = abs(v - mc_value)
            tau = t
    tau_list.append(tau)
    return tau_list

def first_day_distance(solution, time_shift, medicalcase, species):
    distance = float('-inf')
    for s in species:
        mc_first_day, _ = medicalcase.value(s, 0)
        sol_first_day_idx = solution.timepoint_idx_at_time(mc_first_day + time_shift)
        # print('species {}:  timepoint_idx_at_time(mc_first_day {} + time_shift {} = {}) =  sol_first_day_idx {}'.format(s, mc_first_day, time_shift, mc_first_day + time_shift, sol_first_day_idx) )
        if sol_first_day_idx is not None:
            sol_first_day, _ = solution.value(s, sol_first_day_idx)
            # print('sol_first_day {}'.format(sol_first_day))
            sol_first_day -= time_shift
            # print('sol_first_day {}'.format(sol_first_day))
            # print('mc_first_day {}'.format(mc_first_day))
            # print('distance {}'.format(mc_first_day - sol_first_day))
            if mc_first_day < sol_first_day:
                return float('inf')
            if mc_first_day - sol_first_day > distance:
                distance = mc_first_day - sol_first_day
        else:
            return float('inf')
    return distance


class Solution(object):
    def __init__(self, sol, step_size):
        self._logger = logging.getLogger('paeon.solution.Solution')
        self._sol = sol
        self._step_size=step_size
        self._filepath = None

    def concatenate(sol1, sol2):
        for k in sol1._sol.keys():
            sol1._sol[k] = np.concatenate((sol1._sol[k], sol2[k][1:]))
        return sol1

    def load(self, filename, step_size):
        self._sol = np.genfromtxt(filename, dtype='<f8', names=True, deletechars='', delimiter=',', filling_values=float('nan'))
        self._step_size = step_size
        self._filepath = filename
    
    def filepath(self):
        return self._filepath

    def num_timepoints(self):
        return len(self._sol['time'])

    def timepoints(self):
        return self._sol['time']

    def value(self, species, tp_idx):
        return (self._sol['time'][tp_idx], self._sol[species][tp_idx])

    def evolution(self, species):
        return self._sol[species]

    def species_peak(self, species, time_shift=0.0, max_t=None):
        v_max = -1
        argmax = None
        tp_max = None
        if max_t is None:
            max_t = self._sol['time'][self.num_timepoints()-1]
        else:
            max_t += time_shift
        for tp_idx, tp in enumerate(self.timepoints()):
            if tp < time_shift:
                continue
            if tp >= max_t:
                break
            v = self._sol[species][tp_idx]
            if v > v_max:
                v_max = v
                argmax = tp_idx
                tp_max = tp
        return (v_max, tp_max, argmax)

    def timepoint_idx_at_time(self, time):
        if time < self._sol['time'][0] or time > self._sol['time'][-1]:
            return None
        tp_idx = int((time - self._sol['time'][0]) / self._step_size)
        tps = self.num_timepoints()
        counter = 0
        while tp_idx >= tps or self._sol['time'][tp_idx] > time:
            tp_idx -= 1
            counter += 1
        if tp_idx == tps - 1:
            return tp_idx
        while tp_idx < tps - 1 and self._sol['time'][tp_idx + 1] <=time:
            tp_idx += 1
            counter += 1
        if not (self._sol['time'][tp_idx] <= time and time < self._sol['time'][tp_idx + 1]):
            self._logger.error('time({}) = {} and time({} + 1) = {}'.format(tp_idx, self._sol['time'][tp_idx], tp_idx, self._sol['time'][tp_idx + 1]))
            raise errors.WrongTimepointIdxComputation()
        return tp_idx

    
    def store(self, outfile, species_list, timeshift=None, max_t=float('inf'), compress=False):
        if compress:
            out = gzip.open(outfile, 'w')
        else:
            out = open(outfile, 'w')
        time_evol = self.evolution('time')
        if timeshift is not None:
            time_evol = [t + timeshift for t in time_evol if t + timeshift <= max_t]
        species_evol = []
        for s in species_list:
            species_evol.append(self.evolution(s))
        out.write(','.join(['time'] + species_list) + '\n')
        for i,t in enumerate(time_evol):
            values = ['{}'.format(t)]
            for s in species_evol:
                values.append('{}'.format(s[i]))
            out.write(','.join(values) + '\n')
        out.close()



class MedicalCase(Solution):
    def __init__(self, id, filename, species_map=None, step_size=1):
        super(MedicalCase, self).__init__(None, step_size)
        self.id = id
        if species_map is None:
            self._species_map = OrderedDict()
            self._species_map['time'] = 'time'
            self._species_map['gyncycle.hm.LH_blood'] = 'LH'
            self._species_map['gyncycle.hm.FSH_blood'] = 'FSH'
            self._species_map['gyncycle.hm.E2'] = 'E2'
            self._species_map['gyncycle.hm.P4'] = 'P4'
        else:
            self._species_map = species_map
        self._sol = np.genfromtxt(filename, dtype='<f8', names=True, delimiter=',', usecols=(i for i in range(0,len(self._species_map))), filling_values=float('nan'))

    def value(self, species, tp_idx):
        return (self._sol['time'][tp_idx], self._sol[self._species_map[species]][tp_idx])

    def evolution(self, species):
        return self._sol[self._species_map[species]]

    def species_peak(self, species, max_t=None):
            v_max = -1
            argmax = None
            tp_max = None
            if max_t is None:
                max_t = self._sol['time'][self.num_timepoints()-1]
            for tp_idx, tp in enumerate(self.timepoints()):
                if tp >= max_t:
                    break
                v = self._sol[self._species_map[species]][tp_idx]
                if v > v_max:
                    v_max = v
                    argmax = tp_idx
                    tp_max = tp
            return (v_max, tp_max, argmax)
