import ctypes
import tempfile
import time
import shutil
from StringIO import StringIO
import sys
import bisect
import logging
import math
import copy
import numpy as np

from pymodelica import compile_fmu
from pyfmi import load_fmu

from .initstate import InitState
from treatment import Treatment
from .solution import Solution
import errors

class Simulator(object):
    def __init__(self, sampling_time, relative_tolerance=1.0e-6, max_integrator_step=0.1):
        self._logger = logging.getLogger('paeon.simulator.Simulator')
        self._sampling_time = sampling_time
        self._rtol = relative_tolerance
        self._maxh = max_integrator_step

    def simulate(self, initstate=None, treatment=None, start_time=0, final_time=28):
        pass

class Fortran(Simulator):
    def __init__(self, shared_lib, sampling_time, relative_tolerance=1.0e-6, max_integrator_step=0.1):
        self._logger = logging.getLogger('paeon.simulator.Fortran')
        self._shared_lib = shared_lib
        self._sampling_time = sampling_time
        self._rtol = relative_tolerance
        self._maxh = max_integrator_step
        self._paeonlib = ctypes.CDLL(shared_lib)

    def simulate(self, initstate=None, treatment=None, start_time=0, final_time=28):
        pass


class Modelica(Simulator):
    def __init__(self,
                 class_name,
                 file_name,
                 sampling_time,
                 relative_tolerance=1.0e-6,
                 max_integrator_step=0.1, getset=False, initstate=False):
        self._logger = logging.getLogger('paeon.simulator.Modelica')
        self._sampling_time = sampling_time
        self._rtol = relative_tolerance
        self._maxh = max_integrator_step
        try:
            self._fmudir = tempfile.mkdtemp(prefix="paeon.simulator.Simulator.fmu-{}".format(class_name))
            fmu = compile_fmu(class_name,
                                  file_name,
                                  version='2.0',
                                  compile_to=self._fmudir)

        except Exception, e:
            self._logger.error(e)
            raise errors.CompilationError()
        self._model = load_fmu(fmu, log_file_name='/'.join([self._fmudir, 'fmu_log.txt']))
        self._opts = None
        if getset:
            self._model.reset()
            self.__set_initstate(initstate)
            self._opts = self._model.simulate_options()
            start_time = 0
            final_time = initstate.transient_period()
            try:
                self._opts['CVode_options']['maxh'] = self._maxh
                self._opts['CVode_options']['rtol'] = self._rtol
                self._opts['result_handling'] = 'memory'
                self._initialized = True
                old_stdout = sys.stdout
                result = StringIO()
                sys.stdout = result
                self._logger.info('Simulate paeon model from {} to {}, with maxh: {}, rtol: {}, ncp: {}'.format(start_time, start_time + final_time, self._opts['CVode_options']['maxh'], self._opts['CVode_options']['rtol'], self._opts['ncp']))
                simulation_time = time.time()
                res = self._model.simulate(0, final_time, options=self._opts)
                self._time = self._model.time
                simulation_time = time.time() - simulation_time
                self._logger.info('Simulation time {:.3f} (s)'.format(simulation_time))
                sys.stdout = old_stdout
                self._solution = None
            except Exception, e:
                self._logger.error(e)
                raise RuntimeError()

    def __del__(self):
        if self._fmudir is not None:
            shutil.rmtree(self._fmudir)

    def __set_initstate(self, initstate):
        if initstate is None:
            return
        for name, value in initstate.params():
            self._model.set('patientData.{}'.format(name), value)

    def __set_treatment(self, treatment, start_time):
        if treatment is None:
            return
        for i, inj in enumerate(treatment.injections()):
            if i == 0:
                self._model.set('{}.num'.format(inj.drug().name()),
                                len(treatment.injections()))
            self._model.set(
                '{}.arr[{}].timing'.format(inj.drug().name(), i + 1),
                inj.timepoint() + start_time)
            self._model.set('{}.arr[{}].dose'.format(inj.drug().name(), i + 1),
                            inj.dose())
            self._logger.debug('{}.arr[{}].timing={}'.format(inj.drug().name(), i + 1, inj.timepoint() + start_time))
            self._logger.debug('{}.arr[{}].dose={}'.format(inj.drug().name(), i + 1, inj.dose()))

    def get_state(self):
        s = self._model.get_fmu_state()
        return {
            'fmu' : s,
            'initialized': self._initialized,
            'time' : self._time,
            'solution' : copy.deepcopy(self._solution._sol)
        }

    def set_state(self, s):
        self._initialized = s['initialized']
        self._time = s['time']
        self._model.set_time__t(self._time)
        self._model.set_fmu_state(s['fmu'])
        self._solution = Solution(copy.deepcopy(s['solution']), self._sampling_time)

    def free_state(self, s):
        self._model.free_fmu_state(s['fmu'])

    def run(self, duration, treatment=None, t_0=0):
        self.__set_treatment(treatment, t_0)
        self._opts['ncp'] = int(math.ceil(duration / self._sampling_time))
        if self._initialized:
            self._opts['initialize'] = False
            self._model.event_update()
        else:
            self._opts['initialize'] = True
            self._initialized = True
        start_time = self._time
        final_time = start_time + duration
        self._logger.info('Simulate paeon model from {} to {}, with maxh: {}, rtol: {}, ncp: {}'.format(start_time, start_time + final_time, self._opts['CVode_options']['maxh'], self._opts['CVode_options']['rtol'], self._opts['ncp']))
        old_stdout = sys.stdout
        result = StringIO()
        sys.stdout = result
        res = self._model.simulate(start_time, final_time, options=self._opts)
        new_res = {}
        if start_time > 0:
            tp_idx = bisect.bisect(res['time'], start_time) - 1
            new_res['time'] = res['time'][tp_idx:] - start_time
            for v in res.keys():
                new_res[v] = res[v][tp_idx:]
        else:
            new_res = res
        sys.stdout = old_stdout
        self._time = self._model.time
        if self._solution is None:
            self._solution = Solution(new_res, self._sampling_time)
        else:
            self._solution = Solution.concatenate(self._solution, new_res)
        return self._solution

    def simulate(self, initstate=None, treatment=None, start_time=0, final_time=28):
        self._model.reset()
        self.__set_initstate(initstate)
        self.__set_treatment(treatment, start_time)
        self._opts = self._model.simulate_options()
        try:
            self._opts['CVode_options']['maxh'] = self._maxh
            self._opts['CVode_options']['rtol'] = self._rtol
            self._opts['result_handling'] = 'memory'
            self._opts['ncp'] = (start_time + final_time) / self._sampling_time
            self._initialized = True
            old_stdout = sys.stdout
            result = StringIO()
            sys.stdout = result
            self._logger.info('Simulate paeon model from {} to {}, with maxh: {}, rtol: {}, ncp: {}'.format(start_time, start_time + final_time, self._opts['CVode_options']['maxh'], self._opts['CVode_options']['rtol'], self._opts['ncp']))
            simulation_time = time.time()
            res = self._model.simulate(0, start_time + final_time, options=self._opts)
            self._time = self._model.time
            simulation_time = time.time() - simulation_time
            self._logger.info('Simulation time {:.3f} (s)'.format(simulation_time))
            new_res = {}
            if start_time > 0:
                # tp_idx = 0
                tp_idx = bisect.bisect(res['time'], start_time) - 1
                # for i, tp in enumerate(res['time']):
                #     if tp >= start_time:
                #         tp_idx = i
                #         break
                new_res['time'] = res['time'][tp_idx:] - start_time
                for v in res.keys():
                    new_res[v] = res[v][tp_idx:]
            else:
                new_res = res
            sys.stdout = old_stdout
            del result
            self._solution = Solution(new_res, self._sampling_time)
            return self._solution
        except Exception, e:
            self._logger.error(e)
            raise RuntimeError()
