from collections import OrderedDict
import logging

from .errors import *

class InitState(object):
    def __init__(self, vp_id, tau, filename=None):
        self._logger = logging.getLogger('paeon.initstate.InitState')
        self._initstate = OrderedDict()
        self._vp_id = vp_id
        self._transient_period = tau
        if filename is not None:
            self.load(filename)

    def load(self, filename):
        with open(filename) as fp:
            line = fp.readline()
            while line:
                line = fp.readline()
                if line.startswith('#'):
                    continue
                fields = line.strip().split()
                if len(fields) == 1:
                    continue
                if len(fields) == 2:
                    num_params -= 1
                    if num_params < 0:
                        raise InitStateParsingError()
                    param_id = int(fields[0])
                    param_value = float(fields[1])
                    self._initstate['P{}_{}'.format(species_id, param_id)] = param_value
                if len(fields) == 3:
                    species_id = int(fields[0])
                    num_params = int(fields[1])
                    species_value = float(fields[2])
                    self._initstate['P{}'.format(species_id)] = species_value

    def vp_id(self):
        return self._vp_id

    def transient_period(self):
        return self._transient_period
    
    def setParam(self, species_id, param_id, value):
        self._initstate['P{}_{}'.format(species_id, param_id)] = value

    def setSpecies(self, species_id, value):
        self._initstate['P{}'.format(species_id)] = value

    def params(self):
        return self._initstate.items()

    def __str__(self):
        return "id:{}, tau: {}, lambda: {}".format(self._vp_id, self._transient_period, self._initstate.__str__())

    __repr__ = __str__

