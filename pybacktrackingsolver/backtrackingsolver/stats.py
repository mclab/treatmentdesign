import time

class Stats(object):
    def __init__(self, nodes=0, solving_time=0, sol=0):
        self._constraints = {}
        self.clear()
        self._nodes = nodes
        self._solving_time = solving_time
        self._sol = sol

    def start_timer(self):
        self._start_time = time.time()

    def end_timer(self):
        self._stop_time = time.time()
        self._solving_time += self._stop_time - self._start_time
        self._speed = float(self._nodes) / (float(self._solving_time))

    def increment_nodes(self):
        self._nodes += 1

    def increment_solutions(self):
        self._sol += 1

    def nodes(self):
        return self._nodes

    def __iadd__(self, other):
        if isinstance(other, Stats):
            self._nodes += other._nodes
            self._sol += other._sol
            if other._solving_time > self._solving_time:
                self._solving_time = other._solving_time
            self._speed = float(self._nodes) / self._solving_time
            for k, v in self._constraints.iteritems():
                v['sat'] += other._constraints[k]['sat']
                v['unsat'] += other._constraints[k]['unsat']
        else:
            raise TypeError(
                "unsupported operand type(s) for += '{}' and '{}'".format(
                    self.__class__, type(other)))
        return self

    def add_constraint_stats(self, name):
        self._constraints[name] = {}
        self._constraints[name]['sat'] = 0
        self._constraints[name]['unsat'] = 0

    def increment_sat(self, name):
        self._constraints[name]['sat'] += 1

    def increment_unsat(self, name):
        self._constraints[name]['unsat'] += 1

    def clear(self):
        self._solving_time = 0.0
        self._nodes = 0
        self._start_time = 0.0
        self._stop_time = 0.0
        self._speed = 0.0
        self._sol = 0
        for k, v in self._constraints.iteritems():
            v['sat'] = 0
            v['unsat'] = 0

    def copy(self):
        s = Stats()
        s._solving_time = self._solving_time
        s._sol = self._sol
        s._nodes = self._nodes
        s._start_time = self._start_time
        s._stop_time = self._stop_time
        s._speed = self._speed
        s._constraints = self._constraints.copy()
        return s
    
    def __str__(self):
        solver = 'Solver statistics:\n - Solutions: {}\n - Nodes expanded: {}\n - Time: {} (s)\n - Speed {} (nodes/s)\n'.format(self._sol, self._nodes, self._solving_time, self._speed)
        constraints = ''
        for k, v in self._constraints.iteritems():
            constraints += '- Constraint {}:\n - Sat {}\n - Unsat {}\n'.format(k, v['sat'], v['unsat'])
        return '{}{}'.format(solver, constraints)

