import random
import time
import logging
import sys

from . import errors
from .action import Action
from .constraint import Constraint
from .stats import Stats


class State(object):
    def __init__(self, actions, shuffle, start_action=None):
        self._logger = logging.getLogger('backtrackingsolver.solver.State')
        self.actions = actions
        if shuffle:
            random.shuffle(actions)
        if start_action is None:
            self.action_idx = 0
        else:
            try:
                self.action_idx = self.actions.index(start_action)
            except Exception, e:
                self._logger.error(e)
                raise errors.ActionNotFoundError()

    def __setstate__(self, d):
        self.__dict__.update(d)

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['_logger']
        return d
    
    def __str__(self):
        return '{}'.format(self.actions[self.action_idx])
    __repr__ = __str__


class Solver(object):
    def __init__(self, horizon, shuffle=False, max_nodes=float('inf')):
        self._logger = logging.getLogger('backtrackingsolver.solver.Solver')
        self._shuffle = shuffle
        self._h = horizon
        self._L_start = 0
        self._L = horizon
        self._adj_l = 0
        self._layer = 1
        self._actions = []
        self._constraints = []
        self._searchstack = []
        self._stats = Stats()
        self._max_nodes = max_nodes

    def getstate(self):
        return [self._searchstack, self._stats, self._L_start, self._adj_l, self._layer]

    def setstate(self, state):
        self._searchstack = state[0]
        self._stats = state[1]
        self._L_start = state[2]
        self._adj_l = state[3] 
        self._layer = state[4]

    def __iadd__(self, other):
        if isinstance(other, Action):
            self.__add_action(other)
        elif isinstance(other, Constraint):
            self.__add_constraint(other)
        else:
            raise TypeError(
                "unsupported operand type(s) for += '{}' and '{}'".format(
                    self.__class__, type(other)))
        return self

    def incr_search_layer(self):
        if self._adj_l <= 0:
            self._adj_l += 1

    def decr_search_layer(self):
        if self._adj_l == 0:
            self._adj_l -= 1

    def __add_action(self, action):
        self._actions.append(action)

    def __add_constraint(self, constraint):
        self._constraints.append(constraint)
        self._stats.add_constraint_stats('{}'.format(constraint))

    def __init_constraints(self, actions=None):
        if actions is None:
            return
        for c in self._constraints:
            c.initialize(actions)

    def start(self, from_actions=None, L=-1, L_start=None, stats=None):
        if L > -1:
            self._L = max(1, min(self._h, L))
        if not self._actions:
            raise errors.NoActionsError()
        self.reset()

        if from_actions is not None:
            for action in from_actions:
                self._searchstack.append(State([a for a in self._actions], self._shuffle, start_action=action))
            if L_start is None:
                self._L_start = len(from_actions)
            else:
                self._L_start = L_start
        else:
            self._searchstack.append(State([a for a in self._actions], self._shuffle))

        if stats is not None:
            self._stats._nodes = stats._nodes
            self._stats._solving_time = stats._solving_time
            self._stats._sol = stats._sol

        self.__init_constraints(actions=from_actions)
        self._do_backtrack = False

    def solve(self):
        self._logger.info('Start search')
        if not self._searchstack:
            raise errors.SearchStackIsEmpty()
        self._stats.start_timer()
        solution = None
        done = False
        if self._do_backtrack is True:
            self._logger.info('Do backtrack')
            done = self.__next_state(self._do_backtrack)
            self._do_backtrack = False
        while not done:
            self._stats.increment_nodes()
            self._logger.info('Search stack: {}'.format(self._searchstack))
            constraints_sat = self.__check_constraints()
            self._logger.info('Search status: Constraints {}, Search stack len {}, Delegation level {}'.format(constraints_sat, len(self._searchstack), self._L))
            if constraints_sat and len(self._searchstack) == self._L:
                if self._L == self._h:
                    self._stats.increment_solutions()
                solution = self.__get_solution()
                done = True
                self._do_backtrack = True
            else:
                done = self.__next_state(not constraints_sat)
            if self._stats._nodes >= self._max_nodes:
                sys.exit()
        self._stats.end_timer()
        return solution

    def __try_to_adjust_search_layer(self):
        self._logger.debug('Try to adjust search layer: {}, {}, {}'.format(self._adj_l, self._L, self._layer))
        if self._adj_l > 0 or self._L + self._adj_l == self._layer:
            self._L = max(1, min(self._h, self._L + self._adj_l))
            self._adj_l = 0
        self._logger.debug('--> {}, {}, {}'.format(self._adj_l, self._L, self._layer))

    def __get_solution(self):
        return [s.actions[s.action_idx] for s in self._searchstack]

    def __next_state(self, do_backtrack):
        if do_backtrack or len(self._searchstack) >= self._L:
            while len(self._searchstack) > self._L_start:
                state = self._searchstack.pop()
                self._layer -= 1
                self.__try_to_adjust_search_layer()
                if state.action_idx < len(self._actions) - 1:
                    state.action_idx += 1
                    self._searchstack.append(state)
                    self._layer += 1
                    return False
            return True
        else:
            self._searchstack.append(State([a for a in self._actions], self._shuffle))
            self._layer += 1
            return False

    def __check_constraints(self):
        sat = True
        actions = [s.actions[s.action_idx] for s in self._searchstack]
        for c in self._constraints:
            sat = c.check(actions)
            if not sat:
                self._logger.debug('Constraint {} not sat'.format(c))
                self._stats.increment_unsat('{}'.format(c))
                return sat
            self._stats.increment_sat('{}'.format(c))
        return sat

    def stats(self):
        return self._stats.copy()

    def reset(self):
        # self._searchstack.clear()
        self._searchstack = []
        self._stats.clear()
