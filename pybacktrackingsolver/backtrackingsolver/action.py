
class Action(object):

    def __init__(self, name):
        self._name = name

    def __str__(self):
        return self._name

    def __eq__(self, other):
        return self._name == other._name
    
    def __ne__(self, other):
        return not self == other

    __repr__ = __str__
