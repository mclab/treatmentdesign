class InitializationError(Exception):
    pass
class NoActionsError(Exception):
    pass
class SearchStackIsEmpty(Exception):
    pass
class ActionNotFoundError(Exception):
    pass
class SearchLayerOutOfBounds(Exception):
    pass
