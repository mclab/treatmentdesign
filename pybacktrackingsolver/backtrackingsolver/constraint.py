class Constraint(object):
    def __init__(self, name):
        self._name = name

    # @abc.abstractmethod
    def check(self, actions):
        pass

    # @abc.abstractmethod
    def initialize(self, actions):
        pass

    def __str__(self):
        return self._name

    __repr__ = __str__
