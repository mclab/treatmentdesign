from setuptools import setup



setup(name='backtrackingsolver',
        version='1.0',
        description='Backtracking Solver',
        author='Stefano Sinisi',
        author_email='sinisi@di.uniroma1.it',
        packages=['backtrackingsolver']
        )
