# TreatmentDesign

TreatmentDesign is a tool for conducting an In Silico Clinical Trial (ISCT) on human patient digital twins in order to automatic optimise a pharmacological treatment.

TreatmentDesign tool takes as input a Virtual Physiological Human (VPH) model and a set of Virtual Patients (i.e., assignments to the model parameters) representing a human patient digital twin.
Given such patient digital twin, the tool performs extensive simulations (ISCT) guided by an intelligent search aiming at reducing the overall treatment drug amount while keeping the treatment successful for that patient.

For more details please refer to the following publication:

* S. Sinisi, V. Alimguzhin, T. Mancini, E. Tronci, F. Mari, and B. Leeners. _Optimal personalised treatment computation through in silico clinical trials on patient digital twins._ Fundamenta Informaticae, 2020

## How to run

### Requirements

TreatmentDesign depends on the following requirements:

* [MCLab extended version of JModelica.org](https://bitbucket.org/mclab/jmodelica.org). This version provides an implementation to FMI 2.0 functionalities to set and get the internal FMU state.

### Installation

To install TreatmentDesign execute the following instructions:
```
cd treatmentdesign
virtualenv --system-site-packages .venv
source .venv/bin/activate
pip install ../pypaeon
pip install ../pybacktracking
```

## Run

To run TreatmentDesign tool execute the following command:
```
bin/treatmentdesign -i 21,25 -d 21 -s 9 --vps data/digitaltwin-example.csv --t0 84 -o output/
```

## Support

If you have any problems or questions, please feel free to contact us.

## Authors

* [Stefano Sinisi](sinisi@di.uniroma1.it)

## License

TreatmentDesign is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published
by the Free Software Foundation.

TreatmentDesign is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TreatmentDesign.
If not, see <https://www.gnu.org/licenses/>.
